Installation
==================================================

Log into your JTL-Shop Backend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. enter your shop URL/admin in your browser (e.g. www.yourshop.de/admin)
#. enter credentials

.. image:: ./images/admin_login.png


Install plugin
^^^^^^^^^^^^^^
#. go to "Plugins->Pluginverwaltung"

    .. image:: ./images/admin_dashboard.png

#. navigate to "Upload"

.. image:: ./images/plugin_upload.png

#. after clicking on "auswählen" select downloaded plugin ".zip"-file via the filesystem dialog (e.g. "Downloads/360deg-images-jtl-plugin.zip")

    .. image:: ./images/plugin_upload_select.png

#. confirm upload by clicking on "Hochladen"

    .. image:: ./images/plugin_upload_confirm.png

#. navigate to "Verfügbar"
#. select "Q-Two 360Grad-ImageViewer Plugin" by checking the box left to the text

    .. image:: ./images/plugin_select.png

#. click on "Installieren" at the bottom of the page

it should now be shown under the "Aktiviert" tab


