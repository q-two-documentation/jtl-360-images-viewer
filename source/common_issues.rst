Common Issues
==================================================

- Q: when i want to upload an image the shop redirects me to either the login page or the installation successful page.
    - A: you probably have to clear your template cache. For an exact instructions please read here_
.. _here: ./configuration.html#clear-your-shop-and-template-cache

|
| If you encounter any strange or weird behaviour or have any more questions feel free to contact us:
| **Technical support:** florian.becker@q-two.net
| **Legal information:** info@q-two.net