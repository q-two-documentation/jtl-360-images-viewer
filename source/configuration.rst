Configuration
==================================================

Open plugin settings:
^^^^^^^^^^^^^^^^^^^^^^^^^^
#. go to your stores' admin dashboard
#. select "Plugins" tab and click on "Q-Two 360Grad-ImageViewer Plugin" or click on the gears-icon in the "Pluginverwaltung"

| now you should see something like this:

.. image:: ./images/configuration_page.png

| here you can see your images with the corresponding Product ID ( currently there are none).

Clear your template-cache
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. navigate to "System->Cache"

    .. image:: ./images/system_menu.png

#. click on "Gesamten Templatecache leeren" at the bottom of the page

Add 360° image:
^^^^^^^^^^^^^^^^^^^^^^^^^^
#. go to "Add Image" in the plugins' settings page

    .. image:: ./images/plugin_add_image.png

#. now enter a product ID (the one shown in your JTL WaWi)
#. upload your image via a click on browse
#. click "submit"

Delete image
^^^^^^^^^^^^^
| If you want to delete an image click the trash icon on that image in your plugins' settings overview page
.. image:: ./images/plugin_overview_delete.png