.. Q.Two 360°Image-viewer documentation master file, created by
   sphinx-quickstart on Fri Nov 19 10:12:37 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Q.Two 360°Image-viewer's documentation
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   common_issues